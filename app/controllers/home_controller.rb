class HomeController < ApplicationController
  def index
  	if user_signed_in?
  		@grupos_my = Group.where(:person == current_user)
  	end
    @grupos_recentes = Group.all.order("id DESC").limit(8)
  end

  def show
  	@group = Group.find(params[:id])
  	@solicitacoes = Athlete.where(group_id: @group.id, status: "Solicitado")
  	@ativos = Athlete.where(group_id: @group.id, status: "Ativo")
  	@inativos = Athlete.where(group_id: @group.id, status: "Inativo")
  	@convidados = Athlete.where(group_id: @group.id, status: "Convidado")
  end
end
