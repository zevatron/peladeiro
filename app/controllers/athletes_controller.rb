class AthletesController < ApplicationController
  before_action :set_athlete, only: [:show, :edit, :update, :destroy]
  before_action :get_group

  def get_group
    @group = Group.find(params[:group_id])
  end

  # GET /athletes
  # GET /athletes.json
  def index
    @athletes = Athlete.all
  end

  # GET /athletes/1
  # GET /athletes/1.json
  def show
  end

  # GET /athletes/new
  def new
    @athlete = Athlete.new
    if current_user.person.nil? == true
      redirect_to new_person_path , notice: 'Complete seu cadastro antes de se cadastrar como um atleta.'
    end
    @athlete.group = @group
    @athlete.person = current_user.person

  end

  # GET /athletes/1/edit
  def edit
  end

  # POST /athletes
  # POST /athletes.json
  def create
    @athlete = Athlete.new(athlete_params)

    respond_to do |format|
      if @athlete.pendente!
        format.html { redirect_to [@group,@athlete], notice: 'Atleta criado com sucesso.' }
        format.json { render :show, status: :created, location: [@group,@athlete] }
      else
        format.html { render :new }
        format.json { render json: @athlete.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /athletes/1
  # PATCH/PUT /athletes/1.json
  def update
    respond_to do |format|
      if @athlete.update(athlete_params)
        format.html { redirect_to [@group,@athlete], notice: 'Atleta atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: [@group,@athlete] }
      else
        format.html { render :edit }
        format.json { render json: @athlete.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /athletes/1
  # DELETE /athletes/1.json
  def destroy
    @athlete.destroy
    respond_to do |format|
      format.html { redirect_to group_athletes_url, notice: 'Ateta excluido com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_athlete
      @athlete = Athlete.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def athlete_params
      params.require(:athlete).permit(:score, :status, :person_id, :group_id)
    end
end
