json.extract! athlete, :id, :score, :status, :person_id, :group_id, :created_at, :updated_at
json.url athlete_url(athlete, format: :json)
