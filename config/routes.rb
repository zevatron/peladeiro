Rails.application.routes.draw do
  resources :groups do
    resources :events
    resources :athletes
  end
  resources :sports
  resources :people
  devise_for :users
  get 'home/index'
  resources :home
  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
