class DropAthletes < ActiveRecord::Migration[5.1]
  def change
    drop_table :athletes
  end
end
